<Query Kind="Program" />

void Main()
{
	var xml = new XElement("root");
	var child1 = new XElement("child", "I am a child");
	child1.SetAttributeValue("name", "Child1");
	xml.Add(child1);
	xml.ToString().Dump();
}

// Define other methods and classes here