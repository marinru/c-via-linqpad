<Query Kind="Statements" />

var dict = new Dictionary<int, string>()
{
  [3] = "three",
  [10] = "ten",
  [10] = "TEN" // overwrites the value having the same key
};

dict.Dump();