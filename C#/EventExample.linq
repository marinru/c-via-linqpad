<Query Kind="Program" />

void Main()
{
	var publisher = new Publisher();
	publisher.GoodNews += OnGoodNews1;
	publisher.GoodNews += OnGoodNews2;
	publisher.WaitForGoodNewsEvents();
}

public void OnGoodNews1(object sender, EventArgs e)
{
	"Good news everyone!".Dump();
}

public void OnGoodNews2(object sender, EventArgs e)
{
	"Good news guys!".Dump();
}

public class Publisher
{
	public event EventHandler<EventArgs> GoodNews;
	
	public void WaitForGoodNewsEvents()
	{
		for (int i = 0; i < 10; i++)
		{
			i.Dump();
			if (i == 5 || i == 7)
			{
				GoodNews?.Invoke(this, null);
			}
		}
	}
}