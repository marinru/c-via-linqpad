<Query Kind="Program" />

void Main()
{
	var list = new List<int>();
	5.AddTo(list).AddTo(list);
	list.Dump();
	
	5.In(1,2,3,4,5).Dump();
	6.In(1,3,5,7,9).Dump();
}

// Define other methods and classes here
public static class Extensions
{
	public static T AddTo<T>(this T self, ICollection<T> collection)
	{
		collection.Add(self);
		return self;
	}
	
	public static bool In<T>(this T self, params T[] elements)
	{
		return elements.Contains(self);
	}
}