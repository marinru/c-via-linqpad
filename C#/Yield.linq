<Query Kind="Program" />

void Main()
{
	"Enter count:".Dump();
	var count = Convert.ToInt32(Console.ReadLine());
	Fib().Take(count).Dump();
}

// Define other methods and classes here
public IEnumerable<int> Fib()
{
	int a = 1;
	int b = 0;
	
	while (true)
	{
		yield return a;
		b += a;
		a += b;
		b = a - b;
		a = a - b;
	}
}