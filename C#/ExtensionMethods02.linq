<Query Kind="Program" />

void Main()
{
	5.Days().And(2.Hours()).FromNow().Dump();
	10.Days().Ago().Dump();
}

public static class DateTimeHelper
{
	public static TimeSpan Days(this int days)
	{
		return new TimeSpan(days, 0, 0, 0);
	}
	
	public static TimeSpan Hours(this int hours)
	{
		return new TimeSpan(0, hours, 0, 0);
	}
	
	public static TimeSpan And(this TimeSpan timeSpan1, TimeSpan timeSpan2)
	{
		return timeSpan1.Add(timeSpan2);
	}
	
	public static DateTime From(this TimeSpan timeSpan, DateTime dateTime)
	{
		return dateTime.Add(timeSpan);
	}
	
	public static DateTime FromNow(this TimeSpan timeSpan)
	{
		return DateTime.Now.Add(timeSpan);
	}
	
	public static DateTime Ago(this TimeSpan timeSpan)
	{
		return DateTime.Now.Add(-timeSpan);
	}
}