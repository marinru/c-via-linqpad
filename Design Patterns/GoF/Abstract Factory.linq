<Query Kind="Program" />

void Main()
{
    ICountryFactory countryFactory;
    Console.WriteLine("Select a country: 1. Russia, 2. UK, 3. US");
    string choice = Console.ReadLine();
    switch (choice)
    {
        case "1":
            countryFactory = new RussiaFactory(); break;
        case "2":
            countryFactory = new UKFactory(); break;
        case "3":
            countryFactory = new USFactory(); break;
        default:
            Console.WriteLine("Wrong choice"); return;
    }
    
    IMusic music = countryFactory.CreateMusic();
    IVehicle vehicle = countryFactory.CreateVehicle();
    
    Console.WriteLine($"The music is {music}");
    Console.WriteLine($"The vehicle is {vehicle}");
}

// Define other methods and classes here
public interface ICountryFactory
{
    IMusic CreateMusic();
    IVehicle CreateVehicle();
}

public interface IMusic {}

public interface IVehicle {}

// Russia

public class RussiaFactory : ICountryFactory
{
    public IMusic CreateMusic()
    {
        return new RussianMusic();
    }
    
    public IVehicle CreateVehicle()
    {
        return new RussianVehicle();
    }
}

private class RussianMusic: IMusic
{
    public override string ToString()
    {
        return "Золотое кольцо";
    }
}

private class RussianVehicle: IVehicle
{
    public override string ToString()
    {
        return "ВАЗ-2101";
    }
}

// UK

public class UKFactory : ICountryFactory
{
    public IMusic CreateMusic()
    {
        return new UKMusic();
    }
    
    public IVehicle CreateVehicle()
    {
        return new UKVehicle();
    }
}

private class UKMusic: IMusic
{
    public override string ToString()
    {
        return "Beatles";
    }
}

private class UKVehicle: IVehicle
{
    public override string ToString()
    {
        return "Bentley Mark VI";
    }
}

// US

public class USFactory : ICountryFactory
{
    public IMusic CreateMusic()
    {
        return new USMusic();
    }
    
    public IVehicle CreateVehicle()
    {
        return new USVehicle();
    }
}

private class USMusic: IMusic
{
    public override string ToString()
    {
        return "Frank Sinatra";
    }
}

private class USVehicle: IVehicle
{
    public override string ToString()
    {
        return "Chrysler 300C";
    }
}