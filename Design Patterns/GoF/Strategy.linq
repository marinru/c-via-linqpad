<Query Kind="Program" />

void Main()
{
	var vis = new Display(new TextPresenter(), "myText.txt");
	vis.Visualize();
	vis = new Display(new ImagePresenter(), "myImage.jpg");
	vis.Visualize();
	vis = new Display(new HistogramPresenter(), "myBin.bin");
	vis.Visualize();
}

// Presenters

interface IFilePresenter
{
	void PresentFile(string fileName);
}

class TextPresenter : IFilePresenter
{
	public void PresentFile(string fileName)
	{
		Console.WriteLine("Presenting {0} as a text", fileName);
	}
}

class ImagePresenter : IFilePresenter
{
	public void PresentFile(string fileName)
	{
		Console.WriteLine("Presenting {0} as an image", fileName);
	}
}

class HistogramPresenter : IFilePresenter
{
	public void PresentFile(string fileName)
	{
		Console.WriteLine("Presenting {0} as a histogram", fileName);
	}
}

// Display

class Display
{
	private IFilePresenter _filePresenter;
	private string _fileName;
	
	public void Visualize()
	{
		Console.Write("Visualization: ");
		_filePresenter.PresentFile(_fileName);
	}
	
	public Display(IFilePresenter filePresenter, string fileName)
	{
		_filePresenter = filePresenter;
		_fileName = fileName;
	}
}