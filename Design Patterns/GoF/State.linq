<Query Kind="Program" />

static IAtmState state = new WaitingState();
static IList<Account> accounts = new List<Account>()
{
	new Account("Unknown", string.Empty, string.Empty),
	new Account("Ruslan Marin", "1111", "0000")
};
static Account currentAccount = accounts[0];
static IList<Money> moneyInAtm = new List<Money>() { new Money(100000.00M, "643") };

void Main()
{
	state.Logon("1111", "0001");
	state.Insert(new Money(1000, "643"));
	state.Logon("1111", "0000");
	state.Logon("1111", "0000");
	state.Insert(new Money(1000, "640"));
	state.Withdraw(new Money(1000, "643"));
	state.Insert(new Money(1000, "643"));
	state.Withdraw(new Money(900, "643"));
}

// Define other methods and classes here
public interface IAtmState
{
	void Logon(string cardNumber, string pin);
	void Logoff();
	void Insert(Money sum);
	void Withdraw(Money sum);
}

public class WaitingState : IAtmState
{	
	public void Logon(string cardNumber, string pin)
	{
		currentAccount = accounts
			.Where(acc => acc.CardNumber == cardNumber && acc.Pin == pin)
			.SingleOrDefault();
		if (currentAccount != null)
		{
			state = new LoggedOnState();
		}
		else
		{			
			currentAccount = accounts[0];
		}
		
		currentAccount.Dump();
	}
	
	public void Insert(Money sum)
	{
		Console.WriteLine("Can't insert money: log on first.");
	}
	
	public void Withdraw(Money sum)
	{
		Console.WriteLine("Can't withdraw money: log on first.");
	}
	
	public void Logoff()
	{
		Console.WriteLine("Can't log off: nobody logged on.");
	}
}

public class LoggedOnState : IAtmState
{	
	public void Logon(string cardNumber, string pinCode)
	{
		Console.WriteLine("Can't log on: already logged on.");
	}
	
	public void Insert(Money moneyInserted)
	{
		var initialSumInAccount = currentAccount.MoneyInAccount.Sum;
		currentAccount.MoneyInAccount += moneyInserted;
		Console.WriteLine(
			$"{currentAccount.MoneyInAccount.Sum - initialSumInAccount} {currentAccount.MoneyInAccount.Currency.ShortName} added to the account.");
	}
	
	public void Withdraw(Money moneyRequested)
	{
		var initialSumInAccount = currentAccount.MoneyInAccount.Sum;
		var requestedCurrencyinAtm = moneyInAtm.Where(m => m.Currency.Code == moneyRequested.Currency.Code).Single().Sum;
		if (moneyRequested.Sum <= initialSumInAccount
			&& moneyRequested.Sum <= requestedCurrencyinAtm)
		{
			currentAccount.MoneyInAccount -= moneyRequested;
		}
		if (moneyRequested.Sum > initialSumInAccount)
		{
			Console.WriteLine("Insufficient money in account");
		}
		if (moneyRequested.Sum > requestedCurrencyinAtm)
		{
			Console.WriteLine("Insufficient money in ATM");
		}
		Console.WriteLine(
			$"{initialSumInAccount - currentAccount.MoneyInAccount.Sum} {currentAccount.MoneyInAccount.Currency.ShortName} withdrawn from the account.");
	}
	
	public void Logoff()
	{
		Console.WriteLine("Can't log off: nobody logged on.");
	}
}

public class Money
{
	public decimal Sum { get; set; }
	public Currency Currency { get; set; }
	
	public Money(decimal sum, string currencyCode)
	{
		Currency currency = new Currency(currencyCode);
		if (currency != null)
		{
			Sum = sum;
			Currency = currency;
		}
	}
	
	public static Money operator + (Money firstOperand, Money secondOperand)
	{
		if (firstOperand.Currency.Code == secondOperand.Currency.Code)
		{
			firstOperand.Sum += secondOperand.Sum;			
		}
		else
		{
			Console.WriteLine("Can't add a different currency.");
		}
		return firstOperand;
	}
	
	public static Money operator - (Money firstOperand, Money secondOperand)
	{
		if (firstOperand.Currency.Code == secondOperand.Currency.Code)
		{
			firstOperand.Sum -= secondOperand.Sum;			
		}
		else
		{
			Console.WriteLine("Can't subtract a different currency.");
		}
		return firstOperand;
	}
}

public class Currency
{
	public string Code { get; set; }
	public string ShortName => CurrencyCodes[Code];	

	public Currency(string code)
	{
		if (CurrencyCodes.ContainsKey(code))
		{
			Code = code;
		}
		else
		{
			Code = "Unknown";
		}
	}
	
	private IDictionary<string, string> CurrencyCodes = new Dictionary<string, string>
	{
		["Unknown"] = "???",
		["643"] = "руб."
	};
}

public class Account
{
	public string Name { get; private set; }
	public string CardNumber { get; private set; }
	public string Pin { get; private set; }
	public Money MoneyInAccount { get; set; } = new Money(0.00M, "643");
	
	public Account(string name, string cardNumber, string pin)
	{
		Name = name;
		CardNumber = cardNumber;
		Pin = pin;
	}
}